data "google_container_engine_versions" "dev" {
  project        = "${var.k8s_project}"
  location       = "${var.zone}"
  version_prefix = "1.13."
}

resource "google_container_cluster" "homework_cluster" {
  provider = "google-beta.k8s-project"

  name    = "${var.cluster_name}"
  project = "${var.k8s_project}"
  zone    = "${var.zone}"

  initial_node_count       = 1
  min_master_version       = "${data.google_container_engine_versions.dev.latest_master_version}"
  remove_default_node_pool = true
}

resource "google_container_node_pool" "default_pool" {
  provider = "google-beta.k8s-project"

  name               = "node-pool"
  cluster            = "${google_container_cluster.homework_cluster.name}"
  zone               = "${var.zone}"
  initial_node_count = 1
  autoscaling {
    min_node_count     = 1
    max_node_count     = 2
  }
  version            = "${data.google_container_engine_versions.dev.latest_node_version}"

  node_config {
    machine_type = "n1-standard-2"
    disk_size_gb = "50"
    oauth_scopes = "${var.oauth_scopes}"
    preemptible  = true
  }

  timeouts {
    create = "${var.node_pool_creation_timeout}"
  }
}

resource "google_container_node_pool" "client_pool" {
  provider = "google-beta.k8s-project"

  name               = "client-node-pool"
  cluster            = "${google_container_cluster.homework_cluster.name}"
  zone               = "${var.zone}"
  initial_node_count = 1
  autoscaling {
    min_node_count     = 1
    max_node_count     = 20
  }
  version            = "${data.google_container_engine_versions.dev.latest_node_version}"

  node_config {
    machine_type = "n1-highcpu-2"
    disk_size_gb = "50"
    oauth_scopes = "${var.oauth_scopes}"
    preemptible  = true

    labels = {
      load = "client"
    }

    taint = [
      {
        key    = "dedicated"
        value  = "client"
        effect = "NO_SCHEDULE"
      },
    ]
  }

  timeouts {
    create = "${var.node_pool_creation_timeout}"
  }
}

resource "google_container_node_pool" "server_pool" {
  provider = "google-beta.k8s-project"

  name               = "server-node-pool"
  cluster            = "${google_container_cluster.homework_cluster.name}"
  zone               = "${var.zone}"
  initial_node_count = 1
  autoscaling {
    min_node_count     = 1
    max_node_count     = 20
  }
  version            = "${data.google_container_engine_versions.dev.latest_node_version}"

  node_config {
    machine_type = "n1-standard-2"
    disk_size_gb = "50"
    oauth_scopes = "${var.oauth_scopes}"
    preemptible  = true

    labels = {
      load = "server"
    }

    taint = [
      {
        key    = "dedicated"
        value  = "server"
        effect = "NO_SCHEDULE"
      },
    ]
  }

  timeouts {
    create = "${var.node_pool_creation_timeout}"
  }
}
